import { Component } from '@angular/core';
import {TokenParams} from './classes/tokenParams';
import {UsersService} from './shared/services/users.service';
import {UserModel} from './shared/models/user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [UsersService]
})
export class AppComponent {
  title = 'postRequest';

user: UserModel=new UserModel(); // данные вводимого пользователя

receivedUser: UserModel; // полученный пользователь
done: boolean = false;
constructor(private usersService: UsersService ){}
submit(user: UserModel){
  this.usersService.postData(user)
    .subscribe(
      (data: UserModel) => {this.receivedUser=data; this.done=true;},
      error => console.log(error)
    );
}
}
