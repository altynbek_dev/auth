import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {UserModel} from '../models/user.model';


@Injectable()
export class UsersService {
  private headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});

  constructor(private  http: HttpClient) {
  }

  postData(user: UserModel) {
    let body = new URLSearchParams();
    body.set('username', user.username);
    body.set('password', user.password);
    body.set('grant_type', user.grant_type);
    let options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    };
    return this.http.post('http://185.35.222.19:8027/api/Oms/v1/Login/Auth', body, options);
  }
}
